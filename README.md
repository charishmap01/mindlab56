### Assignment for Mind lab 56

This repo consists of two folders

 -- Terraform
    terraform folder contains all of the configuration files required to create the infrastructure on AWS cloud console

 -- Application
    application has the backed app which is a fibonacci app. This app is deployed using the helm template and the docker ecr image. The dockerfile is built and the image is pushed into the ecr repository. This image is later used in the helm template.

 -- Pipeline
    The pipeline has several stages which starts with test stage, Since the app has no test cases defined we have proceeded to next stage which is build and push where the application is built and then pushed to the ECR repository. The next stage is terraform validation followed by plan where the changes are displayed, next stage is terraform apply where the changes are applied automatically since this is set to autoapprove as there are not many users, Finally the application is deployed in eks cluster using helm template and is exposed with a cluster ip service.

    The screen shots of the resources deployed

   ![cluster](../image/image1.png)
   ![pods](../image/image2.png)
   ![service](../image/image3.png)
