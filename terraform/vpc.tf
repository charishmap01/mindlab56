resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "default"
  }
}

resource "aws_subnet" "private-subnet-in-east-1a" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    "Name"          = "private-subnet-in-1a"
  }
}

resource "aws_subnet" "public-subnet-in-east-1b" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone = "us-east-1b"

  tags = {
    "Name"          = "public-subnet-in-1b"
  }
}

resource "aws_internet_gateway" "int_gw" {
  vpc_id = aws_vpc.default.id
}

resource "aws_route_table" "rt_public" {
    vpc_id = aws_vpc.default.id
route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.int_gw.id
    }
tags = {
        Name = "Public Subnet Route Table"
    }
}
resource "aws_route_table_association" "rt_associate_public" {
    subnet_id = aws_subnet.public-subnet-in-east-1b.id
    route_table_id = aws_route_table.rt_public.id
}


resource "aws_eip" "eip" {
  vpc = true
}

resource "aws_nat_gateway" "gw" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public-subnet-in-east-1b.id
}

resource "aws_route_table" "rt_private" {
    vpc_id = aws_vpc.default.id
route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.gw.id
    }
tags = {
        Name = "Main Route Table for Private subnet"
    }
}
resource "aws_route_table_association" "rt_associate_private" {
    subnet_id = aws_subnet.private-subnet-in-east-1a.id
    route_table_id = aws_route_table.rt_private.id
}