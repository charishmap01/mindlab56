/*

	This file defines the provider


*/

provider "aws" {
  region = "us-east-1"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.42"
    }
  }
  required_version = ">= 1.3.3"
}


terraform {
  backend "s3" {
    bucket = "tfstatemindlabs"
    key = "key"
    
    
  }
}

/*********
resource "aws_s3_bucket" "terraform_state" {
  bucket = "tfstate"

}

********/
