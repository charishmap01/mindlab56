

resource "aws_eks_cluster" "cluster_1" {
  name     = "cluster-dev"
  role_arn = aws_iam_role.eks_role.arn

  vpc_config {
    subnet_ids = [
      aws_subnet.private-subnet-in-east-1a.id,
      aws_subnet.public-subnet-in-east-1b.id
    ]
  }


  depends_on = [aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
                aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly-EKS,
                aws_iam_role_policy_attachment.AmazonEKSVPCResourceController]

}

resource "aws_eks_addon" "example" {
  cluster_name = aws_eks_cluster.cluster_1.name
  addon_name   = "vpc-cni"
}

resource "aws_eks_node_group" "cluster_nodes" {
  cluster_name    = aws_eks_cluster.cluster_1.name
  node_group_name = "application-nodes"
  node_role_arn   = aws_iam_role.node_group.arn
  version         = aws_eks_cluster.cluster_1.version

  
  subnet_ids = [
      aws_subnet.private-subnet-in-east-1a.id,
      aws_subnet.public-subnet-in-east-1b.id
  ]
  

   labels = {
    role = "application-node-group"
  }
  
  update_config {
    max_unavailable = 1
  } 

  capacity_type  = "ON_DEMAND"
  instance_types = ["t3.medium"]

  scaling_config {
    desired_size = 1
    max_size     = 2
    min_size     = 0
  }

  
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKSCNIPolicy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]


  
}